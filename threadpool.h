#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <functional>

using namespace std;

class ThreadPool {
private:
    mutex lockWork;
    condition_variable cvWork;
    bool shutdown;
    queue < function <void (void)> > jobs;
    vector <thread> threads;

    mutex lockEmpty;
    condition_variable cvEmpty;
    int nJobs;

public:
    ThreadPool(int nThreads) : shutdown (false) {
        // Create the specified number of threads
        threads.reserve(nThreads);
        for (int i = 0; i < nThreads; ++i)
            threads.emplace_back(bind(&ThreadPool::threadEntry, this, i));
        this->nJobs = nThreads;
    }

    ~ThreadPool () {
        // Tell the threads to shutdown
        {
            unique_lock <mutex> l (lockWork);
            shutdown = true;
            cvWork.notify_all();
        }

        // Wait for all threads to stop
        for (auto& thread : threads)
            thread.join();
    }

    // Emplace a job and notify a worker to execute it
    void emplaceJob(function <void (void)> func) {
        unique_lock <mutex> l (lockWork);
        jobs.emplace(move(func));
        cvWork.notify_one();
    }

    void barrier() {
        unique_lock <mutex> l (lockEmpty);
        if (nJobs != 0) {
            cvEmpty.wait(l);
        }
        this->nJobs = threads.size();
    }

protected:
    void threadEntry(int i) {
        function <void (void)> job;

        while (1) {
            {
                unique_lock <mutex> l (lockWork);

                // Since only one thread is notified, if can be used
                if (!shutdown && jobs.empty())
                    cvWork.wait(l);

                // If unblocked because pool is closing
                if (jobs.empty()) {
                    return;
                }

                job = move(jobs.front());
                jobs.pop();
            }

            job();
            
            {
                unique_lock <mutex> l (lockEmpty);
                nJobs--;
                if (nJobs <= 0) {
                    cvEmpty.notify_one();
                }
            }
        }
    }
};