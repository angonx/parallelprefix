main:
	g++ --std=c++11 main.cpp -lpthread -O3 -Wall
test:
	g++ --std=c++11 tester.cpp -lpthread -O3 -Wall
clear:
	rm -f *csv *out main tester results/*
all:
	g++ --std=c++11 main.cpp -lpthread -O3 -Wall -o main
	g++ --std=c++11 tester.cpp -lpthread -O3 -Wall -o tester
