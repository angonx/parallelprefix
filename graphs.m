speedup = csvread('results/speedup.csv',1);
scalability = csvread('results/scalability.csv',1);
efficiency = csvread('results/efficiency.csv',1);
plot(speedup(:,1), speedup(:,2), 'k-', scalability(:,1), scalability(:,2), 'k:', efficiency(:,1), efficiency(:,2), 'k-.', speedup(:,1), repelem(max(speedup(:,2)), size(speedup(:,1),1)), 'k');
text(125, max(speedup(:,2)+0.1), num2str(max(speedup(:,2))));