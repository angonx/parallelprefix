#include "threadpool.h"
#include <vector>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <thread>

using namespace std;

template<class T> class PP {
private:
    // Parallelism degree
    int nThreads;

public:
    PP(int nThreads)
    {
        // If # of threads has not been specified
        // use the hardware concurrency degree
        this->nThreads = (nThreads < 0) ? thread::hardware_concurrency() : nThreads;
    }

    void setNThreads(int n) {
        if (n > 0)
            this->nThreads = n;
    }

    vector<T> threePhase(vector<T> a, T (*f)(T,T)) {
        ThreadPool threadPool (nThreads);
        int aSize = a.size();
        int nElementsPerThread = ceil(aSize / (double)nThreads);

        // first phase
        vector<T> output (nThreads);
        int i = 0;
        for (int thread = 0; thread < nThreads; ++thread) {
            threadPool.emplaceJob(bind(firstPhase, &output, &a, i, i+min(nElementsPerThread, aSize - i), thread, f));
            i += nElementsPerThread;
        }
        threadPool.barrier();

        // second phase
        // locality principle
        for (int i = 1; i < nThreads; ++i) {
            output[i] = f(output[i], output[i-1]);
        }
        for (int i = nThreads-1; i > 0; --i) {
            output[i] = output[i-1];
        }
        output[0] = 0;

        // third phase
        i = 0;
        for (int thread = 0; thread < nThreads; ++thread) {
            threadPool.emplaceJob(bind(thirdPhase, &output, &a, i, i+min(nElementsPerThread, aSize - i), thread, f));
            i += nElementsPerThread;
        }
        threadPool.barrier();

        return a;
    }

    static void firstPhase(vector<T> * output, vector<T> * input, int i, int bound, int j, T (*f)(T,T)) {
        T tmp = 0;
        for (int k = i; k < bound; ++k) {
            tmp = f(tmp, input->at(k));
        }
        output->at(j) = tmp;
    }

    static void thirdPhase(vector<T> * init, vector<T> * output, int i, int bound, int j, T (*f)(T,T)) {
        if (i < bound) {
            output->at(i) = init->at(j) + output->at(i);
            for (int k = i+1; k < bound; ++k) {
                output->at(k) = f(output->at(k), output->at(k-1));
            }
        }
    }
};

template<class T> vector<T> sequentialPrefix(vector<T> a, T (*f)(T,T))
{
    int aSize = a.size();
    vector<T> output (aSize);

    output[0] = a[0];
    for (int i = 1; i < aSize; ++i)
    {
        output[i] = f(output[i-1], a[i]);
    }

    return output;
}

template<class T> void printVector(vector<T> a)
{
    for (unsigned int i = 0; i < a.size(); ++i)
        cout << a[i] << " ";
    cout << endl;
}