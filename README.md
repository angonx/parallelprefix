
[//]: <> ( ######################################### )

[//]: <> (IF YOU DON'T SEE PARSED THIS DOCUMENT, PLEASE USE THE FOLLOWING WEBSITE AND PASTE THE CONTENT OF THIS FILE INTO THE EDITOR: https://stackedit.io/app#)

[//]: <> ( ######################################### )




# **Parallel Prefix** - *PARIX*
Make the executables (with -O3 optimisation level)
```bash
make all
```
This produces two executables, one for testing the performances and one for testing the correctnes.

For clearing everthing:
```bash
make clear
```

# Correctness Testing
If you want to test the **correctness** use the following command:
```bash
./main -s numberOfThreads -d dimensionOfCollection -i intensityFactor
```
The ```numberOfThreads``` indicates the parallelism degree while the ```dimensionOfCollection``` the dimension of the array in input. The collection is built with ```iota``` function. The ```intensityFactor``` (see the report for more details), represents how much the combiner function (sum) is heavy (in latency terms).

# Performance Stressing
If you want to stress the **performance**, then use the second generated executable in the following manner:
```bash
./tester -s minThread -e maxThread -p stepSize -d dimensionOfCollection -i intensityFactor -t testCases
```
The ```minThread``` indicates the minimum number of threads for the testing, the ```maxThread``` indicates the maximum number of threads for the testing, ```stepSize``` step size from ```minThread``` to ```maxThread```, ```testCases``` the number of test cases > 2 on the same instance. The other parameters are the same of the previous executable.

For example:
```bash
./tester -s 10 -e 50 -p 10 -d 10000 -i 10 -t 5
```
Tests on a collection of 1000 integers 5 times per each of test case. Each test case has the number of workers that goes from 10 to 50 with step size 10, i.e. tests for 10, 20, 30, 40, 50 number of workers. The intensity factor of the sum operation is 5.

The executable will show in output the various completion times and generate in the folder **results** three files, one per each metric, speedup, scalability and efficiency. The files are *csv* format and each line is of type *nThreads, completionTime*.