#include "pp.h"
#include <stdlib.h>
#include <string>
#include <numeric>
#include <fstream>

using namespace std;

int parseArgument(char const * argv[], int argc, char * arg, int * var)
{
    for (int i = 1; i < argc; i += 2)
    {
        if (argv[i][0] == arg[0] && argv[i][1] == arg[1])
        {
            char * endptr;
            const long int value = strtol(argv[i+1], &endptr, 0);
            if (*endptr)
            {
                cerr << "\033[1;31m[ERROR]\033[0m not enought arguments. Usage: \e[1m./generateCSV -s(start) num -e(end) num -p(step) num -i(intensity) num -d(dimension) num -t(test cases > 2) num\e[0m \n";
                return 0;
            }
            *var = value;
            return 1;
        }
    }

    cerr << "\033[1;31m[ERROR]\033[0m not enought arguments. Usage: \e[1m./generateCSV -s(start) num -e(end) num -p(step) num -i(intensity) num -d(dimension) num -t(test cases > 2) num\e[0m \n";
    return 0;
}

int alpha = 0;

long long int sum(long long int x, long long int y)
{
    int t = 0;
    for (int i = 0; i < alpha; ++i) {
        t += pow(2,i);
    }
    return x + y + t;
}

template vector<long long int> sequentialPrefix<long long int>(vector<long long int>, long long int (*f)(long long int, long long int));
template void printVector(vector<long long int>);

int main(int argc, char const *argv[])
{
    // seed random generator
    srand(time(NULL));

    if (argc < 6)
    {
        cerr << "\033[1;31m[ERROR]\033[0m not enought arguments. Usage: \e[1m./generateCSV -s(start) num -e(end) num -p(step) num -i(intensity) num -d(dimension) num -t(test cases > 2) num\e[0m \n";
        return -1;
    }
    
    char argd[] = "-d";
    char args[] = "-s";
    char arge[] = "-e";
    char argp[] = "-p";
    char argi[] = "-i";
    char argt[] = "-t";
    int dim, minThreads, maxThreads, step, intensity, nTestCases;
    int a = parseArgument(argv, argc, argd, &dim);
    int b = parseArgument(argv, argc, args, &minThreads);
    int c = parseArgument(argv, argc, arge, &maxThreads);
    int d = parseArgument(argv, argc, argp, &step);
    int e = parseArgument(argv, argc, argi, &intensity);
    int f = parseArgument(argv, argc, argt, &nTestCases);
    if (!(a && b && c && d && e && f))
    {
        return -1;
    }

    if (nTestCases < 3) {
        cerr << "\033[1;31m[ERROR]\033[0m not enought arguments. Usage: \e[1m./generateCSV -s(start) num -e(end) num -p(step) num -i(intensity) num -d(dimension) num -t(test cases > 2) num\e[0m \n";
        return -1;
    }

    cout << "Testing for vector of dimension " << dim << endl;
    cout << "Computing function with intensity " << intensity << endl << endl;
    alpha = intensity;

    cout << "Building vector... "; cout.flush();

    vector<long long int> vect(dim);
    iota(vect.begin(), vect.end(), 1);
    cout << "done" << endl;

    long long int totalSeqTime = 0;
    cout << "Computing sequentially..."; cout.flush();
    for (int testCase = 1; testCase <= nTestCases; ++testCase) {
        auto begin = chrono::high_resolution_clock::now();
        // ###############################################
        vector<long long int> output = sequentialPrefix(vect, sum);
        // ###############################################
        auto end = chrono::high_resolution_clock::now();
        double elapsed = chrono::duration_cast<chrono::microseconds>(end-begin).count();
        // do not take into consideration outliers
        if (testCase > 1 && testCase < nTestCases)
            totalSeqTime += elapsed;
    }
    totalSeqTime /= (nTestCases-2);
    cout << "done (" << totalSeqTime << " μs ≈ " << (totalSeqTime / 1000) << " ms ≈ " << (totalSeqTime / 1000000) << " s)" << endl; cout.flush();
    
    vector<double> times ((maxThreads - minThreads) / step + 1);
    int i = 0;
    
    PP<long long int> executor (-1);
    for (int nThreads = minThreads; nThreads <= maxThreads; nThreads += step)
    {
        executor.setNThreads(nThreads);
        cout << "Computing with " << nThreads << " threads... "; cout.flush();
        long long int totalTime = 0;
        for (int testCase = 1; testCase <= nTestCases; ++testCase) {
            auto begin = chrono::high_resolution_clock::now();
            // ###############################################
            vector<long long int> output = executor.threePhase(vect, sum);
            // ###############################################
            auto end = chrono::high_resolution_clock::now();
            double elapsed = chrono::duration_cast<chrono::microseconds>(end-begin).count();
            // do not take into consideration outliers
            if (testCase > 1 && testCase < nTestCases)
                totalTime += elapsed;
        }
        totalTime = totalTime / (nTestCases-2);
        cout << "done (" << totalTime << " μs ≈ " << (totalTime / 1000) << " ms ≈ " << (totalTime / 1000000) << " s)" << endl; cout.flush();
        times[i] = totalTime;
        i++;
    }

    ofstream myfile;
    myfile.open("results/speedup.csv");
    i = 0;
    myfile << "threads,speedup" << endl;
    for (int nThreads = minThreads; nThreads <= maxThreads; nThreads += step) {
        double t = totalSeqTime / times[i];
        myfile << nThreads << "," << t << endl;
        i++;
    }
    myfile.close();

    myfile.open("results/scalability.csv");
    i = 0;
    myfile << "threads,scalability" << endl;
    for (int nThreads = minThreads; nThreads <= maxThreads; nThreads += step) {
        double t = times[0] / times[i];
        myfile << nThreads << "," << t << endl;
        i++;
    }
    myfile.close();

    myfile.open("results/efficiency.csv");
    i = 0;
    myfile << "threads,efficiency" << endl;
    for (int nThreads = minThreads; nThreads <= maxThreads; nThreads += step) {
        double t = totalSeqTime / (times[i] * nThreads);
        myfile << nThreads << "," << t << endl;
        i++;
    }
    myfile.close();

    return 0;
}
