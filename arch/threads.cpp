#include "../threadpool.h"
#include <string>
#include <stdlib.h>
#include <string>
#include <numeric>
#include <iostream>
#include <vector>
#include <thread>
#include <unistd.h>

using namespace std;

void f() {
}

int main(int argc, char const *argv[])
{
    int nThreads = 250;
    vector<thread> threads (nThreads);
    auto begin = chrono::high_resolution_clock::now();
    // fork
    for (int i = 0; i < nThreads; ++i) {
        threads[i] = thread(f);
    }
    // join
    for (int i = 0; i < nThreads; ++i) {
        threads[i].join();
    }
    auto end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::nanoseconds>(end-begin).count() << endl;

    begin = chrono::high_resolution_clock::now();
    ThreadPool * pool = new ThreadPool(nThreads);
    for (int i = 0; i < nThreads; ++i) {
        pool->doJob(f);
    }
    pool->await();
    pool->~ThreadPool();
    end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::nanoseconds>(end-begin).count() << endl;
}
