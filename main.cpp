#include "pp.h"
#include <stdlib.h>
#include <string>
#include <numeric>

using namespace std;

int parseArgument(char const * argv[], int argc, char * arg, int * var)
{
    for (int i = 1; i < argc; i += 2)
    {
        if (argv[i][0] == arg[0] && argv[i][1] == arg[1])
        {
            char * endptr;
            const long int value = strtol(argv[i+1], &endptr, 0);
            if (*endptr)
            {
                cerr << "\033[1;31m[ERROR]\033[0m incorrect argument value. Usage: \e[1m./tester -d num -t num -s num\e[0m \n";
                return 0; 
            }
            *var = value;
            return 1;
        }
    }

    cerr << "\033[1;31m[ERROR]\033[0m missing argument " << arg << ". Usage: \e[1m./tester -d num -t num -s num\e[0m \n";
    return 0;
}

int alpha = 1;

long long int sum(long long int x, long long int y)
{
    int t = 0;
    for (int i = 0; i < alpha; ++i) {
        t += pow(2,i);
    }
    return x + y + t;
}

int main(int argc, char const *argv[])
{
    // seed random generator
    srand(time(NULL));

    // usage must be ./tester -d number
    if (argc < 5)
    {
        cerr << "\033[1;31m[ERROR]\033[0m not enought arguments. Usage: \e[1m./main -d collectionSize -s nThreads -i itensityFactor\e[0m \n";
        return -1;
    }
    
    char argd[] = "-d";
    char args[] = "-s";
    char argf[] = "-i";
    int dim, nThreads, intensity;
    int a = parseArgument(argv, argc, argd, &dim);
    int b = parseArgument(argv, argc, args, &nThreads);
    int e = parseArgument(argv, argc, argf, &intensity);
    if (!(a && b && e))
    {
        return -1;
    }


    cout << "Testing for vector of dimension " << dim << endl;
    cout << "Computing function with intensity " << intensity << endl;
    cout << "Number of threads " << nThreads << endl;
    alpha = intensity;

    cout << "Building vector... "; cout.flush();
    vector<long long int> vect(dim);
    iota(vect.begin(), vect.end(), 1);
    cout << "done" << endl;

    cout << "Input vector: ";
    printVector(vect);

    PP<long long int> executor (nThreads);

    vect = executor.threePhase(vect, sum);  

    cout << "Output vector: ";
    printVector(vect);

    return 0;
}
